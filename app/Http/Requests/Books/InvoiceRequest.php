<?php

namespace App\Http\Requests\Books;

use App\Http\Response\ResponseJson;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\Contracts\ConstantsRequest as Constants;

trait InvoiceRequest {

    // Traits
    use ResponseJson;

    protected function sendinvoice($invoice) {
        
        return Http::attach('json', json_encode($invoice->data))->post(Constants::PAC_API);
    }
}