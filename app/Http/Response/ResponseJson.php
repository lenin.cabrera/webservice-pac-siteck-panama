<?php

namespace App\Http\Response;

use Illuminate\Http\JsonResponse;

trait ResponseJson {

    /**
     * success: 200 OK
     * The request was fulfilled.
     * @param string $message
     * @param array $additionalParameter
     * @return \Illuminate\Http\JsonResponse
     */
    public function success(string $message, array $additionalParameter = null): JsonResponse {
        $response = [
            'status'    => true,
            'message'   => $message
        ];

        if (!is_null($additionalParameter)) {
            $response   += $additionalParameter;
        }

        return response()->json($response, 200);
    }

    /**
     * failure: 400 Bad Request
     * The request that was sent has invalid syntax.
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function failure(string $message, array $additionalParameter = null): JsonResponse {
        $response = [
            'status'    => false,
            'message'   => $message
        ];

        if (!is_null($additionalParameter)) {
            $response   += $additionalParameter;
        }
        
        return response()->json($response, 400);
    }

    /**
     * success: 200 OK
     * The request was fulfilled but a warning was sent.
     * @param string $message
     * @param array $additionalParameter
     * @return \Illuminate\Http\JsonResponse
     */
    public function warning(string $message, array $additionalParameter = null): JsonResponse {
        $response = [
            'status'    => true,
            'warning'   => $message
        ];

        if (!is_null($additionalParameter)) {
            $response   += $additionalParameter;
        }
        
        return response()->json($response, 200);
    }
}