<?php

namespace App\Http\Controllers\API\Books;

use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Books\InvoiceRequest;

class InvoiceController extends Controller
{
    // Request
    use InvoiceRequest;

    public function syncInvoice(Request $request) {

        try {

            $invoice = new Invoice;
            $invoice->fillFields($request);
            
            $response = $this->sendinvoice($invoice);

            if (json_decode($response)->codigo_mf_numero == 0) {
                $responsePAC = ['response' => ['CUFE' => json_decode($response)->CUFE]];
                return $this->success("Factura timbrada", $responsePAC);
            }

            if (json_decode($response)->codigo_mf_numero == 2) {
                $errorPAC = ['error' => json_decode(json_decode($response)->MsgRes)];
                return $this->failure("Error detectado por el PAC", $errorPAC);
            }

            return $response;

        } catch (\Throwable $throw) {
            return $this->failure($throw);
        }
    }
}
